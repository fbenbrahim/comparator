Master
-----------

Requirements
~~~~~~~~~~~~

For building:

* Java 7

For running:

* Java 7

Version info
~~~~~~~~~~~~

::

  ./gradlew version

Create Eclipse project
~~~~~~~~~~~~~~~~~~~~~~

::

  ./gradlew eclipse

Note: sometimes the settings are incorrectly set to conform to Java 1.5, which causes a compiling error in the project.

Create IntelliJ project
~~~~~~~~~~~~~~~~~~~~~~~

::

  ./gradlew idea


Run the software
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

graphical/frame/ComparatorMain.java > main