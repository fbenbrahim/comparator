package model.chemicals;

import model.Constants;
import model.OligoGraph;

public class DegradableInputTemplate<E> extends Template<E> {
	
	protected double exo = Constants.exoVm/Constants.exoKmTemplate;
	protected double inputUnstab = 100.0; //should be homogenized with other extensions

	public DegradableInputTemplate(OligoGraph<SequenceVertex, E> parent,
			double totalConcentration, double stackSlowdown, double dangleL,
			double dangleR, SequenceVertex from, SequenceVertex to,
			SequenceVertex inhib) {
		super(parent, totalConcentration, stackSlowdown, dangleL, dangleR, from, to,
				inhib);
	}

	//Convenience method
	public DegradableInputTemplate(Template<E> original){
		super(original.parent,original.totalConcentration(), original.stackSlowdown,
				original.dangleLSlowdown, original.dangleRSlowdown, original.from, original.to,
				original.inhib);
	}
	
	public void setExo(double exo){
		this.exo = exo;
	}
	
	@Override
	public double inputSequenceFlux() {
	    double conc = this.from.getConcentration();
	    double kinhib;
	    double inhib;
	    if (this.inhib != null) {
	      inhib = this.inhib.getConcentration();
	      kinhib = Constants.Kduplex;
	    }
	    else {
	      inhib = 0.0D;
	      kinhib = 0.0D;
	    }
	    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() *inputUnstab* Constants.Kduplex * (
	      dangleLSlowdown*this.concentrationWithInput + stackSlowdown/dangleRSlowdown*this.concentrationWithBoth) + 
	      kinhib * 
	      inhib * 
	      this.concentrationWithInput - 
	      Constants.Kduplex * 
	      conc * (
	      this.concentrationAlone + this.concentrationWithOutput + Constants.ratioToeholdLeft*this.concentrationInhibited);

	    return debug;
	  }
	
	
	@Override
	protected double aloneFlux() {
	    double concIn = this.from.getConcentration();
	    double concOut = this.to.getConcentration();
	    double kreleaseInhib;
	    double concInhib;
	    double selfstart = 0;
	    if(parent.selfStart){
	    	selfstart = Constants.ratioSelfStart;
	    }
	    if (this.inhib != null) {
	      concInhib = this.inhib.getConcentration();
	      kreleaseInhib = ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex;
	    } else {
	      concInhib = 0.0D;
	      kreleaseInhib = 0.0D;
	    }

	    double debug = ((Double)this.parent.K.get(this.from)).doubleValue()*inputUnstab * Constants.Kduplex * 
	      dangleLSlowdown*this.concentrationWithInput + 
	      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
	      this.concentrationWithOutput + 
	      kreleaseInhib * 
	      this.concentrationInhibited - 
	      this.concentrationAlone * 
	      Constants.Kduplex * (
	      concIn + concOut + concInhib)
	      -this.concentrationAlone*selfstart*poly*parent.polConc + this.concentrationWithInput*this.exo;

	    return debug;
	  }
@Override
	  protected double inputFlux() {
	    double concIn = this.from.getConcentration();
	    double concOut = this.to.getConcentration();
	    double concInhib;

	    if (this.inhib != null)
	      concInhib = this.inhib.getConcentration();
	    else {
	      concInhib = 0.0D;
	    }
	    return Constants.Kduplex * 
	      concIn * 
	      (this.concentrationAlone + Constants.ratioToeholdLeft*this.concentrationInhibited) + 
	      ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
	      stackSlowdown/dangleLSlowdown* this.concentrationWithBoth - 
	      this.concentrationWithInput * (
	      dangleLSlowdown*((Double)this.parent.K.get(this.from)).doubleValue() *inputUnstab* Constants.Kduplex + this.poly*parent.polConc + Constants.Kduplex * 
	      concOut + Constants.Kduplex * 
	      concInhib) - this.concentrationWithInput*this.exo;
	  }
@Override
	  protected double outputFlux()
	  {
	    double concIn = this.from.getConcentration();
	    double concOut = this.to.getConcentration();
	    double concInhib;
	    double selfstart = 0;
	    if(parent.selfStart){
	    	selfstart = Constants.ratioSelfStart;
	    }
	    if (this.inhib != null)
	      concInhib = this.inhib.getConcentration();
	    else {
	      concInhib = 0.0D;
	    }
	    double debug = concOut * 
	      Constants.Kduplex * 
	      (this.concentrationAlone + Constants.ratioToeholdRight*this.concentrationInhibited) + 
	      ((Double)this.parent.K.get(this.from)).doubleValue() *inputUnstab* Constants.Kduplex * 
	      stackSlowdown/dangleRSlowdown* this.concentrationWithBoth - 
	      this.concentrationWithOutput * (
	      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + Constants.Kduplex * concIn + Constants.Kduplex * 
	      concInhib)
	      + this.concentrationAlone*selfstart*poly*parent.polConc + this.concentrationWithBoth*this.exo;

	    return debug;
	  }
@Override
	  protected double bothFlux() {
	    double concIn = this.from.getConcentration();
	    double concOut = this.to.getConcentration();
	    double pol = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
	      this.poly;
	    pol*=parent.polConc;
	    return Constants.Kduplex * concIn * this.concentrationWithOutput + 
	      Constants.Kduplex * concOut * this.concentrationWithInput + 
	      this.nick * parent.nickConc * this.concentrationExtended - 
	       this.concentrationWithBoth * (((Double)this.parent.K.get(this.from)).doubleValue()*inputUnstab * Constants.Kduplex * stackSlowdown/dangleRSlowdown
	    		   + stackSlowdown/dangleLSlowdown* ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + pol)
	    		   -this.concentrationWithBoth*this.exo;
	  }
}
