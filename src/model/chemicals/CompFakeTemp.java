package model.chemicals;

import model.Constants;
import model.OligoGraph;

public class CompFakeTemp<E> extends Template<E> {
	
	//This represents two compared sequences together.
	//We only use the inputConcentration

	public double totalStab = 1.0; //We have a specific stability
	
	//The input and output have identical roles, so the func is the same.
	
	public CompFakeTemp(OligoGraph<SequenceVertex, E> parent, SequenceVertex from, SequenceVertex to) {
		super(parent, 0.0, 1.0, 1.0, 1.0, from, to,null);
		this.numberOfStates = 1;
		//System.out.println("Generated: "+from+" "+to);
	}
	
	@Override
	public double[] flux() {
		
	    double[] ans = {inputFlux()};
	    return ans;
	  }
	
	@Override
	public double[] getStates() {
		
	    double[] ans = {concentrationWithInput};
	    return ans;
	  }
	
	
	public double inputSequenceFlux() {
	    double conc = this.from.getConcentration();
	    double concOut = this.to.getConcentration();
	   
	    double debug = this.concentrationWithInput*Constants.Kduplex*totalStab-Constants.Kduplex*conc*concOut;

	    return debug;
	  }

	  public double outputSequenceFlux() {
		  double conc = this.from.getConcentration();
		    double concOut = this.to.getConcentration();
		   
		    double debug = this.concentrationWithInput*Constants.Kduplex*totalStab-Constants.Kduplex*conc*concOut;

	    return debug;
	  }
	  
	  
	  @Override
	  protected double inputFlux() {
	   
		  double conc = this.from.getConcentration();
		    double concOut = this.to.getConcentration();
		   
		    double debug = -this.concentrationWithInput*(Constants.Kduplex*totalStab+this.poly*parent.polConc)+Constants.Kduplex*conc*concOut;
		    
		    return debug;
	  }
	
	@Override
	 public void setStates(double[] values) throws InvalidConcentrationException {
		    if (values.length != numberOfStates) {
		      System.err
		        .println("Error: wrong internal values setting for template " + 
		        this);
		      return;
		    }
		    for (int i = 0; i < numberOfStates; i++) {
		      if ((values[i] < 0.0D) || (values[i] > this.totalConcentration+1))
		      {
		        values[i] = 0.0D;
		      }
		    }

		    this.concentrationWithInput = values[0];
	}
	
	@Override
	public String toString(){
		return "C"+from+"+"+to;
	}
	
	public String getName()
	  {
		  return "comp";
	  }

}
