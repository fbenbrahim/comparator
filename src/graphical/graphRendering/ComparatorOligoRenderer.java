package graphical.graphRendering;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.transform.shape.GraphicsDecorator;
import model.ComparatorGraph;

public class ComparatorOligoRenderer<V, E> extends OligoRenderer<V, E> {
	
	public void render(RenderContext<V, E> renderContext, Layout<V, E> layout) {
		super.render(renderContext, layout); //Paint everything else
		
		//Paint all comparator
		ArrayList<ArrayList<V>> comps = ((ComparatorGraph<V,E>) layout.getGraph()).getAllComparators();
		for(ArrayList<V> c : comps){
			renderComparator(renderContext,layout,c);
		}
		
	}
	
	public void renderComparator(RenderContext<V, E> rc, Layout<V, E> layout, ArrayList<V> verts){
		V v1, v2;
		Point2D p1,p2;
		double x1,y1,x2,y2;
		
		GraphicsDecorator g = rc.getGraphicsContext();
		
		for(int i = 0; i<verts.size()-1; i++){
			v1 = verts.get(i);
			for(int j=i+1; j<verts.size(); j++){
				v2 = verts.get(j);
				//Draw a double barheaded line between the two V
				p1 = layout.transform(v1);
				p2 = layout.transform(v2);

				p1 = rc.getMultiLayerTransformer().transform(Layer.LAYOUT, p1);
				p2 = rc.getMultiLayerTransformer().transform(Layer.LAYOUT, p2);
				
				x1 =  0.7 * p1.getX() + 0.3 * p2.getX();
				x2 =  0.3 * p1.getX() + 0.7 * p2.getX();
				y1 =  0.7 * p1.getY() + 0.3 * p2.getY();
				y2 =  0.3 * p1.getY() + 0.7 * p2.getY();
				
				GeneralPath instance = new GeneralPath();
				instance.moveTo(x1, y1);
				instance.lineTo(x2, y2);
				Shape edgeShape = instance;
				
				//Draw the barheads
				
				instance = new GeneralPath();
				instance.moveTo(-1, 0);
				instance.lineTo(1, 0);
				Shape endShape1 = instance;
				AffineTransform xform = AffineTransform.getTranslateInstance(x1, y1);
				double dx = x1 - x2;
				double dy = y1 - y2;
				float thetaRadians = (float) Math.atan2(dy, dx);
				xform.rotate(thetaRadians+Math.PI/2);
				xform.scale(4, 0.0);
				endShape1 = xform.createTransformedShape(endShape1);
				instance = new GeneralPath();
				instance.moveTo(-1, 0);
				instance.lineTo(1, 0);
				Shape endShape2 = instance;
				xform = AffineTransform.getTranslateInstance(x2, y2);
				xform.rotate(thetaRadians+Math.PI/2);
				xform.scale(4, 0.0);
				endShape2 = xform.createTransformedShape(endShape2);
				
				Paint oldPaint = g.getPaint();
				
				Paint fill_paint = Color.black;
				if (fill_paint != null) {
					g.setPaint(fill_paint);
					g.fill(edgeShape);
					g.fill(endShape1);
					g.fill(endShape2);
				}
				Paint draw_paint = Color.black;
				if (draw_paint != null) {
					g.setPaint(draw_paint);
					g.draw(edgeShape);
					g.draw(endShape1);
					g.draw(endShape2);
				}

				// restore old paint
				g.setPaint(oldPaint);
			}
		}
	}

}
