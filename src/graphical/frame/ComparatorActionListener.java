package graphical.frame;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.ProgressMonitor;

import model.ComparatorGraph;
import model.ComparatorOligoSystem;
import model.Constants;
import model.OligoSystem;
import model.chemicals.SequenceVertex;
import edu.uci.ics.jung.algorithms.layout.Layout;
import utils.MyWorker;

public class ComparatorActionListener<V> extends MyActionlistener<V, String> {

	
	public ComparatorActionListener(Layout<V, String> layout, DataPanel<String> data) {
		super(layout, data);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void plot() {
		
		model = new ComparatorOligoSystem((ComparatorGraph)this.graph);
		
		final MyWorker myWorker = new MyWorker(model,graph);
		
		ProgressMonitor progressMonitor = new ProgressMonitor(null,
	            "Simulating",
	            "", 0, 100);
		progressMonitor.setProgress(0);

		final ProgressMonitor cop = progressMonitor;
		
		myWorker.setProg(cop);
		myWorker.setCustomProgress(0);
		myWorker.addPropertyChangeListener(new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				//System.out.println(evt.getPropertyName());
				if ("progress".equals(evt.getPropertyName()) ) {
					//System.out.println("Balsdkbgn");
		            int progress = (Integer) evt.getNewValue();
		            cop.setProgress(progress);
				}
				
			}
			
		});
		
		myWorker.execute();
	}
	
	@Override
	protected void exportTimeSerie() {
		// TODO Auto-generated method stub
		model = new ComparatorOligoSystem((ComparatorGraph<SequenceVertex, String>) this.graph);
		double[][] res = model.calculateTimeSeries(null);
		String export = "#";
		for(int i=0;i<this.graph.getVertexCount();i++){
			export += "s"+i+"\t";
		}
		if(Constants.fullExport){
		Iterator<String> it = model.templates.keySet().iterator();
		while(it.hasNext()){
			String e = it.next();
			export += ""+e+"alone"+"\t"+e+"in"+"\t"+e+"out"+"\t"+e+"both"+"\t"+e+"ext"+"\t"+e+"inhibited"+"\t";
		}
		}
		export += "\n";
		
		for(int t=0;t<res[0].length;t++){
			for(int i=0;i<(Constants.fullExport?res.length:graph.getVertexCount());i++){
				export+= ""+ res[i][t] + "\t";
			}
			export+= "\n";
		}
		JFileChooser exp = new JFileChooser();
		int result = exp.showSaveDialog(null);
		if (result == JFileChooser.APPROVE_OPTION){
			File selectedFile = exp.getSelectedFile();
			try {
				if (!selectedFile.exists()){

					selectedFile.createNewFile();
				
				}
				
				FileWriter fw = new FileWriter(selectedFile);
				fw.write(export);
				fw.flush();
				fw.close();
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}
}
