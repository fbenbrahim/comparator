package graphical.frame;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import model.ComparatorGraph;
import model.OligoGraph;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.Layer;

public class ComparatorVisualizationServer<V,E> extends MyVisualizationServer<V, E> {

	public ComparatorVisualizationServer(Layout<V, E> layout,
			GraphicInterfaceMouseAdapter mouse, DataPanel data) {
		super(layout, mouse, data);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void rightclic(Point point) {
		Point2D p;
		AffineTransform affine;
		JPopupMenu pm = new JPopupMenu();
		final ComparatorGraph<V,E> graph = ((ComparatorGraph<V,E>) layout.getGraph());
		for (V v: graph.getVertices()){
			p = layout.transform(v);
			p = this.getRenderContext().getMultiLayerTransformer().transform(Layer.LAYOUT, p);
			affine = AffineTransform.getTranslateInstance(p.getX(), p.getY());
			if (affine.createTransformedShape(this.getRenderContext().getVertexShapeTransformer().transform(v)).contains(point))
			{
				
				JLabel title = new JLabel(""+v);
				pm.add(title);
				pm.add(new JSeparator());
				JMenuItem menuitem = new JMenuItem("Delete");
				menuitem.setMnemonic(KeyEvent.VK_D);
				menuitem.getAccessibleContext().setAccessibleDescription(
			        "Delete Sequence");
				menuitem.setActionCommand("delete");
				final V selected = v;
				menuitem.addActionListener(new ActionListener(){
		        	public void actionPerformed(ActionEvent e){
		        		
		        		graph.getSelected().add(selected);
		        		ArrayList<V> selec = new ArrayList<V>(graph.getSelected());
		        		for (V i: selec){
		        			if(graph.getVertices().contains(i))
		        				graph.removeVertex(i);
		        			 
		        		 }
		        		graph.getSelected().clear();
		        		 data.update();
		        		 data.getParent().repaint();
		        		 graph.replot(); //reploted once
		        	}
				});
				pm.add(menuitem);
				menuitem = new JMenuItem("Add input");
				menuitem.setActionCommand("addinput");
				menuitem.addActionListener(new ActionListener(){
		        	public void actionPerformed(ActionEvent e){
		        		InputManageWindow<V> imw = new InputManageWindow<V>(selected,(OligoGraph<V,E>)layout.getGraph());
		        		imw.addWindowListener(new WindowAdapter(){
		        			@Override
		        			public void windowClosing(WindowEvent arg0) {
		        				data.getParent().repaint();
		        			}
		        		});
		        		imw.setVisible(true);
		        		imw.setAlwaysOnTop(true);
		        	}
				});
				pm.add(menuitem);
				
				if(graph.getSelected().size()>=3){
					menuitem = new JMenuItem("Set comparator");
					menuitem.setActionCommand("addComp");
					menuitem.addActionListener(new ActionListener(){

						@Override
						public void actionPerformed(ActionEvent e) {
							ArrayList<V> s = new ArrayList<V>();
							for(V v : graph.getSelected()){
								s.add(v);
							}
							graph.addComparator(s);
						}
						
					});
					pm.add(menuitem);
				}
				pm.show(this,(int) Math.round(point.getX()),(int) Math.round(point.getY()));
				break;
			}
		}
		
	}
}
